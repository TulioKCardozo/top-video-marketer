# README #

This is the README file for TopVideoMarketer.com's website repo. 

### What is this repository for? ###

* Repo includes all files for plugins, images, and theme files. This repo does NOT include WordPress Core files.
* 0.0.0

### How do I get set up? ###

* To deploy, push code to WPEngine.
* Make sure to have your SSH public key registed with WPEngine for the install (topvideomarket)
* To setup your machine, follow:
  cd ~/wordpress/my_wp_install_name
  $ git remote add production git@git.wpengine.com:production/topvideomarket.git
* Next run
  $ git remote -v
* You should see:
  production  git@git.wpengine.com:production/topvideomarket.git (fetch)
  production  git@git.wpengine.com:production/topvideomarket.git (push)
* To deploy :
  $ git push production master
  OR -
  $ $ git push staging master
* Pull from Production or Staging:
  $ git pull production master
  $ git pull staging master

### Contribution guidelines ###

* Please request a code review form Tulio K. Cardozo

### Who do I talk to? ###

* Please speak with Tulio K. Cardozo if you have any questions.
* tulio@flexwp.com