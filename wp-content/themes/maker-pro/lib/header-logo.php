<?php
/**
 * Header Customizations
 *
 * Required functions for the theme.
 *
 * @package     Maker Pro
 * @subpackage  Genesis
 * @license     GPL-2.0+
 * @since       2.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

//* Add modified header logo for SEO optimization
add_filter( 'genesis_seo_title', 'tvmpro_genesis_seo_site_title', 10, 2 );

function tvmpro_genesis_seo_site_title( $title, $inside ){
	// Setting $scheme = 'http' can help with mixed http/https front-end URLs
	// Change it to 'https' if your site is using https
	$home_url = home_url( $path = '', $scheme = 'http' );
	
	$child_inside = sprintf( 
		'<a href="%s" title="%s">
			<img 	src="'. get_stylesheet_directory_uri() .'/images/top-video-marketer-logo.png" 
					srcset="' . get_stylesheet_directory_uri() . '/images/top-video-marketer-logo@2x.png 2x" 
					title="%s" 
					alt="%s" 
					width="300px" 
					height="37px"/>
		</a>', 
		trailingslashit( $home_url ), 
		esc_attr( get_bloginfo( 'name' ) ), 
		esc_attr( get_bloginfo( 'name' ) ), 
		esc_attr( get_bloginfo( 'name' ) ) );
 
	$title = str_replace( $inside, $child_inside, $title );
 
	return $title;
	
}

//* Remove site description
remove_action( 'genesis_site_description', 'genesis_seo_site_description' );