jQuery(function( $ ){

	// start the auto carousel
	$(window).scroll(function(){

		$('#slider1').tinycarousel({
			interval: true,
			intervaltime: 1,
			duration: 1,
			buttons   : false
		});

		var slider1 = $("#slider1").data("plugin_tinycarousel")
		
		// Start the slider interval
		slider1.start();

	});

});