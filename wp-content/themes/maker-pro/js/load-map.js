jQuery(function( $ ) {

	// Load google maps

	// Latitude and longitude of the location
var position = [34.1565586, -118.6458569];

    function initialize() {

        var myOptions = {
            zoom: 14,
            streetViewControl: true,
            scaleControl: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById('google-maps'),
            myOptions);


        latLng = new google.maps.LatLng(position[0], position[1]);

        map.setCenter(latLng);

        marker = new google.maps.Marker({
            position: latLng,
            map: map,
            draggable: false,
            animation: google.maps.Animation.DROP
        });


    }

    google.maps.event.addDomListener(window, 'load', initialize);


  //   Disabling this code for now but the the thing I need to code out is an on click handler that turns on the 

  //   // Disable scroll zooming and bind back the click event
  // var onMapMouseleaveHandler = function (event) {
  //   var that = $(this);

  //   that.on('click', onMapClickHandler);
  //   that.off('mouseleave', onMapMouseleaveHandler);
  //   that.find('#google-maps').css("pointer-events", "none"); 
  //   // pointer-events needs to be added as a style on the iframe
  // }

  // var onMapClickHandler = function (event) {
  //   var that = $(this);

  //   // Disable the click handler until the user leaves the map area
  //   that.off('click', onMapClickHandler);

  //   // Enable scrolling zoom
  //   that.find('#google-maps').css("pointer-events", "auto");

  //   // Handle the mouse leave event
  //   that.on('mouseleave', onMapMouseleaveHandler);
  // }

  // // Enable map zooming with mouse scroll when the user clicks the map referencing map container
  // $('#google-maps').on('click', onMapClickHandler);


        // Disable scroll zooming and bind back the click event
  var onMapMouseleaveHandler = function (event) {
    var that = $(this);

    that.on('click', onMapClickHandler);
    that.off('mouseleave', onMapMouseleaveHandler);
    that.find('iframe').css("pointer-events", "none"); 
    // pointer-events needs to be added as a style on the iframe
  }

  var onMapClickHandler = function (event) {
    var that = $(this);

    // Disable the click handler until the user leaves the map area
    that.off('click', onMapClickHandler);

    // Enable scrolling zoom
    that.find('iframe').css("pointer-events", "auto");

    // Handle the mouse leave event
    that.on('mouseleave', onMapMouseleaveHandler);
  }

  // Enable map zooming with mouse scroll when the user clicks the map referencing map container
  $('.google-maps').on('click', onMapClickHandler);
});