<?php
/**
 * This file adds the About Us Page to the Maker Pro Theme.
 *
 * @author JT Grauke
 * @package Maker Pro
 * @subpackage Customizations
 */



//* Enqueue scripts
add_action( 'wp_enqueue_scripts', 'maker_enqueue_about_style' );
function maker_enqueue_about_style() {

	wp_enqueue_style( 'maker-front-styles', get_stylesheet_directory_uri() . '/style-about.css', array(), CHILD_THEME_VERSION );
	wp_enqueue_script( 'magnific-popup', get_stylesheet_directory_uri() . '/js/magnific-popup.js', array( 'jquery' ), '1.0.0' );
	wp_enqueue_script( 'popup-set', get_stylesheet_directory_uri() . '/js/home-popup-set.js', array( 'jquery' ), '1.0.0' );

}

//* Enque scripts for backstretch
add_action( 'wp_enqueue_scripts', 'trip_anthology_category_page_enqueue_scripts' );
function trip_anthology_category_page_enqueue_scripts() {

	$header_img = get_option( 'tvm_header_image', sprintf( '%s/images/about-header-image.jpg', get_stylesheet_directory_uri() ) );

	//* Enqueue Backstretch scripts
	wp_enqueue_script( 'tvm_backstretch', get_stylesheet_directory_uri() . '/js/backstretch.js', array( 'jquery'), '1.0.0' );
	wp_enqueue_script( 'tvm_backstretch_set', get_stylesheet_directory_uri() . '/js/backstretch-set.js', array( 'jquery', 'tvm_backstretch'), '1.0.0');

	//* If hero section has content and an image is defined, backstretch it
	if ( ! empty( $header_img ) ) {
		wp_localize_script( 'tvm_backstretch_set', 'BackStretchAboutImg', array( 'src' => str_replace( 'http:', '', $header_img ) ) );
	}

}

//* Add front-page body class
add_filter( 'body_class', 'maker_body_class' );
function maker_body_class( $classes ) {

	$classes[] = 'about-page';
	return $classes;

}

//* Force full width content layout
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

//* Remove breadcrumbs
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

//* Add widgets on front page
add_action( 'genesis_after_header', 'maker_about_page_zones' );

//* Remove the default Genesis loop
remove_action( 'genesis_loop', 'genesis_do_loop' );

//* Remove .site-inner
add_filter( 'genesis_markup_site-inner', '__return_null' );
add_filter( 'genesis_markup_content-sidebar-wrap_output', '__return_false' );
add_filter( 'genesis_markup_content', '__return_null' );
add_theme_support( 'genesis-structural-wraps', array( 'header', 'footer-widgets', 'footer' ) );

//* Add widgets on front page
function maker_about_page_zones() {

	echo '<h2 class="screen-reader-text">' . __( 'Main Content', 'maker' ) . '</h2>';

	echo '<div id="about-title-section" class="about-title-section"><div class="wrap">';
	echo '<h1>About Us</h1>';
	echo '</div></div>';

	echo '<div id="whats-an-explainer-video" class="html-zone-1"><div class="wrap">';
	echo the_field('first_html_zone');
	echo '</div></div>';

	echo '<div class="html-zone-2"><div class="wrap">';
	echo the_field('second_html_zone');
	echo '</div></div>';

	echo '<div class="html-zone-3"><div class="wrap">';
	echo the_field('third_html_zone');
	echo '</div></div>';
}


//* Run the Genesis function
genesis();