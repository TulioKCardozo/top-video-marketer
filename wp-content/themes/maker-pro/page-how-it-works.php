<?php
/**
 * This file adds the How It Works Page to the Maker Pro Theme.
 *
 * @author JT Grauke
 * @package Maker Pro
 * @subpackage Customizations
 */

//* Enqueue scripts
add_action( 'wp_enqueue_scripts', 'maker_enqueue_about_style' );
function maker_enqueue_about_style() {

	wp_enqueue_style( 'maker-front-styles', get_stylesheet_directory_uri() . '/style-how-it-works.css', array(), CHILD_THEME_VERSION );

}

//* Add front-page body class
add_filter( 'body_class', 'maker_body_class' );
function maker_body_class( $classes ) {

	$classes[] = 'how-it-works-page';
	return $classes;

}

//* Force full width content layout
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

//* Remove breadcrumbs
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

//* Add widgets on front page
add_action( 'genesis_after_header', 'maker_about_page_zones' );

//* Remove the default Genesis loop
remove_action( 'genesis_loop', 'genesis_do_loop' );

//* Remove .site-inner
add_filter( 'genesis_markup_site-inner', '__return_null' );
add_filter( 'genesis_markup_content-sidebar-wrap_output', '__return_false' );
add_filter( 'genesis_markup_content', '__return_null' );
add_theme_support( 'genesis-structural-wraps', array( 'header', 'footer-widgets', 'footer' ) );

//* Add widgets on front page
function maker_about_page_zones() {

	echo '<h2 class="screen-reader-text">' . __( 'Main Content', 'maker' ) . '</h2>';

	echo '<div id="how-it-works-title-section" class="how-it-works-title-section"><div class="wrap">';
	echo '<h1>How It Works</h1>';
	echo '<h4>It\'s a simple process!</h4>';
	echo '</div></div>';

	echo '<div class="html-zone-1"><div class="wrap">';
	echo the_field('first_html_zone');
	echo '</div></div>';

	echo '<div class="html-zone-2"><div class="wrap">';
	echo the_field('second_html_zone');
	echo '</div></div>';

	echo '<div class="html-zone-3"><div class="wrap">';
	echo the_field('third_html_zone');
	echo '</div></div>';

	echo '<div class="html-zone-4"><div class="wrap">';
	echo the_field('fourth_html_zone');
	echo '</div></div>';

	echo '<div class="html-zone-5"><div class="wrap">';
	echo the_field('fifth_html_zone');
	echo '</div></div>';

	echo '<div class="html-zone-6"><div class="wrap">';
	echo the_field('sixth_html_zone');
	echo '</div></div>';

	echo '<div class="html-zone-7"><div class="wrap">';
	echo the_field('seventh_html_zone');
	echo '</div></div>';

	echo '<div class="html-zone-8"><div class="wrap">';
	echo the_field('eight_html_zone');
	echo '</div></div>';
}

//* Run the Genesis function
genesis();