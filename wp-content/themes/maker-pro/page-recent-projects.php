<?php
/**
 * This file adds the Recent Projects Page to the Maker Pro Theme.
 *
 * @author JT Grauke
 * @package Maker Pro
 * @subpackage Customizations
 */

//* Enqueue scripts
add_action( 'wp_enqueue_scripts', 'maker_enqueue_recent_projects_style' );
function maker_enqueue_recent_projects_style() {

	wp_enqueue_style( 'maker-front-styles', get_stylesheet_directory_uri() . '/style-recent-projects.css', array(), CHILD_THEME_VERSION );
	wp_enqueue_script( 'tiny-carousel', get_stylesheet_directory_uri() . '/js/jquery.tinycarousel.js', array( 'jquery' ), '1.0.0'  );
	wp_enqueue_script( 'tiny-carousel-set', get_stylesheet_directory_uri() . '/js/carousel-set.js', array( 'jquery' ), '1.0.0'  );

}

//* Enque scripts for backstretch
add_action( 'wp_enqueue_scripts', 'trip_anthology_category_page_enqueue_scripts' );
function trip_anthology_category_page_enqueue_scripts() {

	$header_recent_projects_img = get_option( 'tvm_contact_image', sprintf( '%s/images/recent-projects-header-image.jpg', get_stylesheet_directory_uri() ) );
	$header_testimonials_img = get_option( 'tvm_contact_image', sprintf( '%s/images/testimonials-image.jpg', get_stylesheet_directory_uri() ) );

	//* Enqueue Backstretch scripts
	wp_enqueue_script( 'tvm_backstretch', get_stylesheet_directory_uri() . '/js/backstretch.js', array( 'jquery'), '1.0.0' );
	wp_enqueue_script( 'tvm_backstretch_set', get_stylesheet_directory_uri() . '/js/backstretch-set-recent-projects.js', array( 'jquery', 'tvm_backstretch'), '1.0.0');

	//* If hero section has content and an image is defined, backstretch it
	if ( ! empty( $header_recent_projects_img ) ) {
		wp_localize_script( 'tvm_backstretch_set', 'BackStretchRecentProjectsImg', array( 'src' => str_replace( 'http:', '', $header_recent_projects_img ) ) );
		wp_localize_script( 'tvm_backstretch_set', 'BackStretchTestimonialsImg', array( 'src' => str_replace( 'http:', '', $header_testimonials_img ) ) );
	}

}

//* Add front-page body class
add_filter( 'body_class', 'maker_body_class' );
function maker_body_class( $classes ) {

	$classes[] = 'recent-projects-page';
	return $classes;

}

//* Force full width content layout
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

//* Remove breadcrumbs
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

//* Add widgets on front page
add_action( 'genesis_after_header', 'maker_recent_projects_page_zones' );

//* Remove the default Genesis loop
remove_action( 'genesis_loop', 'genesis_do_loop' );

//* Remove .site-inner
add_filter( 'genesis_markup_site-inner', '__return_null' );
add_filter( 'genesis_markup_content-sidebar-wrap_output', '__return_false' );
add_filter( 'genesis_markup_content', '__return_null' );
add_theme_support( 'genesis-structural-wraps', array( 'header', 'footer-widgets', 'footer' ) );

//* Add widgets on front page
function maker_recent_projects_page_zones() {

	echo '<h2 class="screen-reader-text">' . __( 'Main Content', 'maker' ) . '</h2>';

	echo '<div id="recent-projects-title-section" class="recent-projects-title-section"><div class="wrap">';
	echo '<h1>Recent Projects</h1>';
	echo '</div></div>';

	echo '<div class="html-zone-1"><div class="wrap">';
	echo the_field('first_html_zone');
	echo '</div></div>';

	echo '<div class="html-zone-2"><div class="wrap">';
	echo the_field('second_html_zone');
	echo '</div></div>';

	echo '<div id="testimonials-title-section" class="testimonials-title-section"><div class="wrap">';
	echo '<h3>Testimonails</h3>';
	echo '</div></div>';

	echo '<div class="html-zone-3"><div class="wrap">';
	echo the_field('third_html_zone');
	echo '</div></div>';
}

//* Run the Genesis function
genesis();